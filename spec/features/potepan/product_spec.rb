require 'rails_helper'

RSpec.feature "products", type: :feature do
  let!(:product)           { create(:product, taxons: [taxon]) }
  let (:taxonomy)          { create(:taxonomy) }
  let (:taxon)             { create(:taxon, taxonomy: taxonomy) }
  let!(:related_products)  { create_list(:product, 5, taxons: [taxon]) }
  let!(:unrelated_product) { create(:product) }

  before do
    visit potepan_product_path(product.id)
  end

  it "The product name and price are displayed" do
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end

  it "Back to the list page, to the category page" do
    within(".media-body") do
      click_on "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end

  it "Return to the home screen with the HOME button" do
    within(".breadcrumb") do
      click_on "Home"
      expect(current_path).to eq potepan_path
    end
  end

  it "Jump to the individual page of the product with the related product" do
    expect(page).to have_link related_products.first.name
    click_link related_products.first.name
    expect(current_path).to eq potepan_product_path(related_products.first.id)
    expect(page).to have_selector ".productBox", count: 4
  end
  
  it "Don't display unrelated product and main product in related area" do
    within(:css, '.productsContent') do
      expect(page).not_to have_content unrelated_product.name
    end
    within(:css, '.productsContent') do
      expect(page).not_to have_content product.name
    end
  end
end