require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'show' do
    let(:taxon)             { create(:taxon) }
    let(:product)           { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

    before do
      get :show, params: {id: product.id}
    end
    
    it "responds successfully" do
      expect(response).to be_successful
    end

    it "renders the :show template" do
      expect(response).to render_template :show
    end

    it "locates the requested @product" do
      expect(assigns(:product)).to eq product
    end

    it "locates the requested @related_products" do
      expect(assigns(:related_products)).to eq related_products[0..3]
    end
  end
end