require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "category page" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon)    { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let(:products) { create_list(:product, 3, taxons: [taxon]) }

    before do
      get :show, params: {id: taxon.id}
    end

    it "The render method works correctly" do
      expect(response).to render_template :show
    end

    it "The HTTP request completes successfully" do
      expect(response).to be_successful
    end
      
    it "has correct instance variable" do
      expect(assigns(:taxonomies).first).to eq taxon.root
      expect(assigns(:taxon)).to eq taxon
      expect(assigns(:products)).to eq products
    end
  end
end
